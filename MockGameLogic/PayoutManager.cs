﻿using System;

namespace MockGameLogic
{
	public class PayoutManager
	{
		public static void Payout(Player player, int payout)
		{
			player.ChipBalance += payout;
		}
	}
}

