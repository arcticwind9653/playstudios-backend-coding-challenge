﻿using System;

namespace MockGameLogic
{
	public class Player
	{
		private string userId;
		private int chipBalance;

		public Player(string userId, int chipBalance)
		{
			this.userId = userId;
			this.chipBalance = chipBalance;
		}

		public string UserId
		{
			get
			{
				return this.userId;
			}
			set
			{
				this.userId = value;
			}
		}

		public int ChipBalance
		{
			get
			{
				return this.chipBalance;
			}
			set
			{
				this.chipBalance = value;
			}
		}
	}
}

