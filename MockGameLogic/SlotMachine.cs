﻿using System;

namespace MockGameLogic
{
	public delegate void ReelSpinnedHandler(object sender, IntEventArgs e);
	public delegate void WonHandler(object sender, IntEventArgs e);
	public delegate void WonJackpotHandler(object sender, IntEventArgs e);

	public class SlotMachine
	{
		private const int REEL_COUNT = 3;
		private const int REEL_ELEMENT_COUNT = 10;
		private const int CHIP_PER_SPIN = 1;
		private const int WIN_PAYOUT = 1000;
		private const int WIN_JACKPOT_PAYOUT = 10000;

		private Random random;

		public event ReelSpinnedHandler ReelSpinned;
		public event WonHandler Won;
		public event WonJackpotHandler WonJackPot;

		public SlotMachine()
		{
			random = new Random();	
		}

		/// <summary>
		/// Randomly generate an array of integer that represents the result of a slot machine
		/// Will also broadcast an event that the reels had been spinned.
		/// </summary>
		public int[] Spin(Player player)
		{
			//deduce 1 chip from player
			PayoutManager.Payout(player, -1 * CHIP_PER_SPIN);

			int[] result = new int[REEL_COUNT];
			for (int i = 0; i < REEL_COUNT; i++)
			{
				result[i] = random.Next(0, REEL_ELEMENT_COUNT);
			}

			if (ReelSpinned != null)
				ReelSpinned(this, new IntEventArgs(player, 1));

			if (IsWinningJackpot(result))
			{
				PayoutManager.Payout(player, WIN_JACKPOT_PAYOUT);
				if (WonJackPot != null)
					WonJackPot(this, new IntEventArgs(player, 1));
			}
			else if (IsWinningResult(result))
			{
				PayoutManager.Payout(player, WIN_PAYOUT);
				if (Won != null)
					Won(this, new IntEventArgs(player, 1));
			}

			return result;
		}

		private bool IsWinningJackpot(int[] result)
		{
			//Jackpot is 777
			return (IsWinningResult(result) && result[0] == 7);
		}

		private bool IsWinningResult(int[] result)
		{
			//simple check only, when all numbers are equal
			bool allEquals = true;
			for (int i = 1; i < result.Length; i++)
			{
				if (result[i-1] != result[i])
					allEquals = false;
			}
			return allEquals;
		}
	}
}

