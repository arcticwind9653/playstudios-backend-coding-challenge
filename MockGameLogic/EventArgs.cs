﻿using System;

namespace MockGameLogic
{
	public class IntEventArgs : EventArgs
	{
		private Player player;
		private int increment;

		public IntEventArgs(Player player, int increment)
		{
			this.player = player;
			this.increment = increment;
		}

		public Player Player
		{ 
			get
			{
				return this.player;
			}
		}
		public int Increment
		{
			get
			{
				return this.increment;
			}
		}
	}
}

