﻿using System.Data.SQLite;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using MockGameLogic;

namespace AchievementManager
{
	[DataContract]
	public class IntAchievement : Achievement
	{
		public IntAchievement(int achievementId, string name, string description, int targetMultiplier, string triggerName, Reward reward) : base(achievementId, name, description, targetMultiplier, triggerName, reward)
		{
		}

		public async Task<int> GetProgress(string userId)
		{
			//read from DB
			int progress = 0;
			using (SQLiteConnection con = new SQLiteConnection(AchievementManager.DbConnectionString))
			{
				SQLiteCommand cmd = new SQLiteCommand();
				cmd.CommandText = @"SELECT Progress FROM AchievementProgresses WHERE UserId = @userId AND AchievementId = @achievementId LIMIT 1;";
				cmd.Connection = con;
				cmd.Parameters.Add(new SQLiteParameter("@userId", userId));
				cmd.Parameters.Add(new SQLiteParameter("@achievementId", this.AchievementId));
				await con.OpenAsync();
				using (var reader = await cmd.ExecuteReaderAsync())
				{
					if (await reader.ReadAsync())
					{
						progress = (int)reader["Progress"];
					}
					else
					{
						//if record doesn't exist, create one
						await CreateAchievementProgressRecord(userId);
					}
				}
			}
			return progress;
		}

		public async void OnTriggered(object sender, IntEventArgs e)
		{
			await this.UpdateProgress(e.Player, e.Increment);
		}

		protected async Task UpdateProgress(Player player, int increment)
		{
			int progress = await GetProgress(player.UserId);
			if (progress == this.TargetMultiplier)	//already completed this achievement
				return;
			if (progress < this.TargetMultiplier)
			{
				//update progress value in DB
				progress += increment;
				using (SQLiteConnection con = new SQLiteConnection(AchievementManager.DbConnectionString))
				{
					SQLiteCommand cmd = new SQLiteCommand();
					cmd.CommandText = @"UPDATE AchievementProgresses SET Progress = @progress WHERE UserId = @userId AND AchievementId = @achievementId;";
					cmd.Connection = con;
					cmd.Parameters.Add(new SQLiteParameter("@progress", progress));
					cmd.Parameters.Add(new SQLiteParameter("@userId", player.UserId));
					cmd.Parameters.Add(new SQLiteParameter("@achievementId", this.AchievementId));
					await con.OpenAsync();
					await cmd.ExecuteNonQueryAsync();
				}
			}
			if (progress >= this.TargetMultiplier) //check if achievement is completed
			{
				AchievementEventArgs achievementEventArgs = new AchievementEventArgs(player, this.Reward);
				//update DB, set the completed flag and datetime
				using (SQLiteConnection con = new SQLiteConnection(AchievementManager.DbConnectionString))
				{
					SQLiteCommand cmd = new SQLiteCommand();
					cmd.CommandText = @"UPDATE AchievementProgresses SET CompletedDateTime = @completedDateTime WHERE UserId = @userId AND AchievementId = @achievementId;";
					cmd.Connection = con;
					cmd.Parameters.Add(new SQLiteParameter("@completedDateTime", achievementEventArgs.CompletedDateTime));
					cmd.Parameters.Add(new SQLiteParameter("@userId", player.UserId));
					cmd.Parameters.Add(new SQLiteParameter("@achievementId", this.AchievementId));
					await con.OpenAsync();
					await cmd.ExecuteNonQueryAsync();
				}

				//broadcast the AchievementCompleted event
				base.OnAchievementCompleted(this, achievementEventArgs);
			}
		}

		private async Task<int> CreateAchievementProgressRecord(string userId)
		{
			int result = 0;
			using (SQLiteConnection con = new SQLiteConnection(AchievementManager.DbConnectionString))
			{
				SQLiteCommand cmd = new SQLiteCommand();
				cmd.CommandText = @"INSERT INTO AchievementProgresses (UserId, AchievementId, Progress, CompletedDateTime) VALUES (@userId, @achievementId, 0, NULL);";
				cmd.Connection = con;
				cmd.Parameters.Add(new SQLiteParameter("@userId", userId));
				cmd.Parameters.Add(new SQLiteParameter("@achievementId", this.AchievementId));
				await con.OpenAsync();
				result = await cmd.ExecuteNonQueryAsync();
			}
			return result;
		}
	}
}

