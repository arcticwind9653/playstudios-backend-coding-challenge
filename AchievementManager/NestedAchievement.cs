﻿using System.Threading.Tasks;
using MockGameLogic;
using System.Data.SQLite;
using System.Runtime.Serialization;

namespace AchievementManager
{
	[DataContract]
	public class NestedAchievement : IntAchievement
	{
		private Achievement[] subAchievements;
		private int[] subAchievementIds;

		public NestedAchievement(int achievementId, string name, string description, Reward reward, params int[] subAchievementIds) : base(achievementId, name, description, 0, null, reward)
		{
			this.subAchievementIds = subAchievementIds;
		}

		[DataMember(Name = "subAchievementIds")]
		public int[] SubAchievementIds
		{
			get
			{
				return this.subAchievementIds;
			}
			set
			{
				this.subAchievementIds = value;
			}
		}

		public Achievement[] SubAchievements
		{
			get
			{
				return this.subAchievements;
			}
			set
			{
				this.subAchievements = value;
				foreach (Achievement subAchievement in this.subAchievements)
				{
					if (subAchievement is IntAchievement) {
						((IntAchievement)subAchievement).AchievementCompleted += UpdateProgress;
					}
					//TODO: for other type of achievement, add handling logic here
				}
			}
		}

		private async void UpdateProgress (object sender, AchievementEventArgs e)
		{
			await base.UpdateProgress(e.Player, 1);
		}
	}
}

