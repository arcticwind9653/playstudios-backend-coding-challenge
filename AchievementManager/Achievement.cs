﻿using System;
using System.Runtime.Serialization;

namespace AchievementManager
{
	public delegate void AchievementCompletedHandler(object sender, AchievementEventArgs e);

	[DataContract]
	public class Achievement
	{
		public event AchievementCompletedHandler AchievementCompleted;

		private int achievementId;
		private string name;
		private string description;
		private int targetMultiplier;
		private string triggerName;
		private Reward reward;

		public Achievement(int achievementId, string name, string description, int targetMultiplier, string triggerName, Reward reward)
		{
			this.achievementId = achievementId;
			this.name = name;
			this.description = description;
			this.targetMultiplier = targetMultiplier;
			this.triggerName = triggerName;
			this.reward = reward;
		}

		[DataMember(Name = "achievementId")]
		public int AchievementId
		{
			get
			{
				return this.achievementId;
			}
			set
			{
				this.achievementId = value;
			}

		}

		[DataMember(Name = "name")]
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		[DataMember(Name = "description")]
		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		[DataMember(Name = "targetMultiplier")]
		public int TargetMultiplier
		{
			get
			{
				return this.targetMultiplier;
			}
			set
			{
				this.targetMultiplier = value;
			}
		}

		[DataMember(Name = "triggerName")]
		public string TriggerName
		{
			get
			{
				return this.triggerName;
			}
			set
			{
				this.triggerName = value;
			}
		}

		[DataMember(Name = "reward")]
		public Reward Reward
		{
			get
			{
				return this.reward;
			}
			set
			{
				this.reward = value;
			}
		}

		protected virtual void OnAchievementCompleted(object sender, AchievementEventArgs e)
		{
			if (this.AchievementCompleted != null)
			{
				this.AchievementCompleted(sender, e);
			}
		}
	}
}

