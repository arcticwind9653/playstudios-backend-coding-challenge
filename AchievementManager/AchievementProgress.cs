﻿using System;
using MockGameLogic;

namespace AchievementManager
{
	public class AchievementProgress
	{
		private Player player;
		private Achievement achievement;
		private decimal progressPercentage;

		//TODO: some other constructors for handling different progress types other than int
		public AchievementProgress(Player player, Achievement achievement, int progress)
		{
			this.player = player;
			this.achievement = achievement;
			this.progressPercentage =  progress * 100.0M / this.achievement.TargetMultiplier;
		}

		public Player Player
		{
			get
			{
				return this.player;
			}
		}

		public Achievement Achievement
		{
			get
			{
				return this.achievement;
			}
		}

		public decimal ProgressPercentage
		{
			get
			{
				return this.progressPercentage;
			}
		}
	}
}

