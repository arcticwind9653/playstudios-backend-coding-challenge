﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Json;
using MockGameLogic;
using System.Data;
using System.Data.SQLite;


namespace AchievementManager
{
	public class AchievementManager
	{
		public static string DbConnectionString = "Data Source = ..\\..\\..\\DB\\MockGame.db; Version = 3;";

		private static AchievementManager instance;

		private List<Achievement> achievements;

		private AchievementManager()
		{
		}

		public static AchievementManager Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new AchievementManager();
				}
				return instance;
			}
		}

		public void Init(SlotMachine slotMachine)
		{
			//Use JSON to read the list of achievements
			DataContractJsonSerializerSettings settings = new DataContractJsonSerializerSettings();
			List<Type> knowTypes = new List<Type>();
			knowTypes.Add(typeof(IntAchievement));
			knowTypes.Add(typeof(NestedAchievement));
			settings.KnownTypes = knowTypes;
			DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<Achievement>), settings);

			using (FileStream fs = File.Open("achievements.json", FileMode.Open))
			{
				fs.Position = 0;
				this.achievements = (List<Achievement>)ser.ReadObject(fs);
			}

			foreach (Achievement achievement in this.achievements)
			{
				if (achievement is NestedAchievement)
				{
					List<Achievement> subAchievements = new List<Achievement>();
					foreach (int subAchievementIds in ((NestedAchievement)achievement).SubAchievementIds)
					{
						subAchievements.Add(achievements.Find(a => a.AchievementId == subAchievementIds));
					}
					((NestedAchievement)achievement).SubAchievements = subAchievements.ToArray();
				}

				//register event of slotmachine based on the TriggerName of the achievement
				//for nested achievement, no need to register since it has no TriggerName
				if (!string.IsNullOrEmpty(achievement.TriggerName))
				{
					EventInfo triggerEvent = typeof(SlotMachine).GetEvent(achievement.TriggerName);
					if (triggerEvent != null)
					{
						Delegate handler = Delegate.CreateDelegate(triggerEvent.EventHandlerType, achievement, "OnTriggered");
						triggerEvent.AddEventHandler(slotMachine, handler);
					}
				}

				achievement.AchievementCompleted += HandleAchievementCompleted;
			}
		}

		public AchievementProgress[] GetAllAchievementProgresses(Player player)
		{
			DataTable dt = new DataTable();
			using (SQLiteConnection con = new SQLiteConnection(AchievementManager.DbConnectionString))
			{				
				SQLiteCommand cmd = new SQLiteCommand();
				cmd.CommandText = @"SELECT AchievementId, Progress FROM AchievementProgresses WHERE UserId = @userId";
				cmd.Connection = con;
				cmd.Parameters.Add(new SQLiteParameter("@userId", player.UserId));
				con.Open();
				using (SQLiteDataAdapter da = new SQLiteDataAdapter(cmd))
				{
					da.Fill(dt);
				}
			}
			AchievementProgress[] achievementProgresses = new AchievementProgress[this.achievements.Count];
			for (int i = 0; i < this.achievements.Count; i++)
			{
				int progress;
				DataRow[] rows = dt.Select("AchievementId=" + this.achievements[i].AchievementId);
				if (rows.Length == 0)
				{
					progress = 0;
				}
				else
				{					
					progress = (int)rows[0]["Progress"];
				}
				AchievementProgress achievementProgress = new AchievementProgress(player, this.achievements[i], progress);
				achievementProgresses[i] = achievementProgress;
			}
			return achievementProgresses;
		}

		private void HandleAchievementCompleted (object sender, AchievementEventArgs e)
		{
			Console.WriteLine(e.Player.UserId + ", " + e.Reward.Message + " You get " + e.Reward.Payout + " chips as reward!");
			PayoutManager.Payout(e.Player, e.Reward.Payout);

			//TODO: this should be implemented with more sophisticated logics such as DB operation, or broadcast another event for frontend system to handle.
		}
	}
}

