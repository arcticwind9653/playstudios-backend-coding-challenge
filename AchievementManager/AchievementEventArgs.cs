﻿using System;
using MockGameLogic;

namespace AchievementManager
{
	public class AchievementEventArgs: EventArgs
	{
		private Player player;
		private DateTime completedDateTime;
		private Reward reward;

		public AchievementEventArgs(Player player, Reward reward)
		{
			this.player = player;
			this.reward = reward;
			this.completedDateTime = DateTime.Now;
		}

		public Player Player
		{
			get
			{
				return this.player;
			}
		}

		public Reward Reward
		{
			get
			{
				return this.reward;
			}
		}

		public DateTime CompletedDateTime
		{ 
			get
			{
				return this.completedDateTime;
			}
		}
	}
}

