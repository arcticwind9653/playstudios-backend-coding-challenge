﻿using System;
using System.Runtime.Serialization;

namespace AchievementManager
{
	[DataContract]
	public class Reward
	{
		private string message;
		private int payout;

		public Reward()
		{			
		}

		[DataMember(Name = "message")]
		public string Message
		{
			get
			{
				return this.message;
			}
			set
			{
				this.message = value;
			}
		}

		[DataMember(Name = "payout")]
		public int Payout
		{
			get
			{
				return this.payout;
			}
			set
			{
				this.payout = value;
			}
		}
	}
}

