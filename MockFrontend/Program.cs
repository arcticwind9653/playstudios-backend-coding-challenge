﻿using System;
using AchievementManager;
using MockGameLogic;

namespace MockFrontend
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("press 's' to spin as UserA, 'p' to spin as UserB, 'Spacebar' to check user info");
			Console.WriteLine("'x' to exit");

			Player userA = new Player("userA", 1000);
			Player userB = new Player("userB", 1000);

			SlotMachine slotMachine = new SlotMachine();
			AchievementManager.AchievementManager achievementManager = AchievementManager.AchievementManager.Instance;
			achievementManager.Init(slotMachine);

			ConsoleKeyInfo info;
			do
			{
				info = Console.ReadKey(true);

				if (info.KeyChar == 's')
				{
					int[] result = slotMachine.Spin(userA);
					for (int i = 0; i < result.Length; i++)
					{
						Console.Write(result[i] + " ");
					}
					Console.WriteLine();
				}

				if (info.KeyChar == 'p')
				{
					int[] result = slotMachine.Spin(userB);
					for (int i = 0; i < result.Length; i++)
					{
						Console.Write(result[i] + " ");
					}
					Console.WriteLine();
				}

				if (info.KeyChar == ' ')
				{
					Console.WriteLine("================================");
					Console.WriteLine("{0} has {1} chips", userA.UserId, userA.ChipBalance);
					Console.WriteLine("{0}'s achievement progresses:", userA.UserId);
					AchievementProgress[] userAAchievementProgresses = achievementManager.GetAllAchievementProgresses(userA);
					foreach(AchievementProgress achievementProgress in userAAchievementProgresses)
					{
						Console.WriteLine("{0}    {1}% done", achievementProgress.Achievement.Name, achievementProgress.ProgressPercentage);
					}
					Console.WriteLine("--------------------------------");
					Console.WriteLine("{0} has {1} chips", userB.UserId, userB.ChipBalance);
					Console.WriteLine("{0}'s achievement progresses:", userB.UserId);
					AchievementProgress[] userBAchievementProgresses = achievementManager.GetAllAchievementProgresses(userB);
					foreach(AchievementProgress achievementProgress in userBAchievementProgresses)
					{
						Console.WriteLine("{0}    {1}% done", achievementProgress.Achievement.Name, achievementProgress.ProgressPercentage);
					}
					Console.WriteLine("================================");
				}

				if (info.KeyChar == 'x')
					break;
			} while (true);
		}
	}
}
